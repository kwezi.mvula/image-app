@extends('pages.layout')
   
@section('content')
   <div class="content">
	<div class="container">
		<div class="content-top">
            <div class="pull-left">
				<h2 class="new">Information Details</h2>
            </div>
            <div class="pull-right">
            <a class="btn btn-info" href="{{ route('information.index') }}"><i class="fa fa-arrow-circle-left"></i></a>
        </div>
			<div class="wrap">
				<div class="main">
                    <div class="container-fluid">
                        <center>
                            <div class="col-md-12">
                                <div class="card card-chart">
                                    <div class="card-header card-header-success">
                                        <div id="dailySalesChart"><img class="mx-auto d-block" src="{{ URL::to($information->image) }}" height="245px;" width="320px;"   alt=""/></div>
                                    </div>
                                    <div class="card-body">
                                        <i class="fa fa-hashtag" style="font-size:24px; color:#8fbf00">{{ $information->id }}</i><h4 class="card-title">{{ $information->name }}</h4>
                                        <p class="card-category">
                                        <span class="text-success"></span> {!! html_entity_decode($information->type) !!}</p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="stats">
                                            <form action="{{ route('information.destroy',$information->id) }}" method="POST">
                                                <a class="btn btn-primary" href="{{ route('information.edit',$information->id) }}"><i class="fa fa-edit"></i></a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </center>
                    </div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection