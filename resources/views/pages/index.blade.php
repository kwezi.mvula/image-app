@extends('pages.layout')
     
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('information.create') }}"> <i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="col-lg-12 margin-tb">
        <table class="table table-bordered">
            <tr>
                <th></th>
                <th>Image</th>
                <th>Name</th>
                <th>Type</th>
                <th width="280px"></th>
            </tr>
            @foreach ($information as $info)
            <tr>
                <td>{{ $info->id }}</td>
                <td><img src="{{ URL::to($info->image) }}" width="100px"></td>
                <td>{{ $info->name }}</td>
                <td>{{ $info->type }}</td>
                <td>
                    <form action="{{ route('information.destroy',$info->id) }}" method="POST">
     
                        <a class="btn btn-info" href="{{ route('information.show',$info->id) }}"><i class="fa fa-eye"></i></a>
      
                        <a class="btn btn-primary" href="{{ route('information.edit',$info->id) }}"><i class="fa fa-edit"></i></a>
     
                        @csrf
                        @method('DELETE')
        
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
        </div>
    
    {!! $information->links() !!}
        
@endsection