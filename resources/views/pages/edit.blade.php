@extends('pages.layout')
     
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Information Details</h2>
            </div>
            <div class="pull-right">
                  <a class="btn btn-info" href="{{ route('information.index') }}"><i class="fa fa-arrow-circle-left"></i></a>
            </div>
        </div>
    </div>
     
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('information.update',$information->id) }}" method="POST" enctype="multipart/form-data"> 
        @csrf
        @method('PUT')
     
         <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <input type="text" name="name" value="{{ $information->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Type:</strong>
                    <td>
                        <select class="custom-select" name="type" id="type">
                            <option selected>{{ $information->type }}</option>
                            <option value="Stuff Image">Stuff Image</option>
                            <option value="Product Image">Product Image</option>
                        </select>
                    </td>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Image:</strong>
                    <input type="file" name="image" class="form-control" placeholder="image">
                    <input type="hidden"  class="custom-file-input" name="old_image" value="{{ $information->image }}">
                    <img src="{{ URL::to($information->image) }}" width="300px">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i></button>
            </div>
        </div>
     
    </form>
@endsection