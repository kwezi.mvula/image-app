<?php

namespace App\Http\Controllers;

use App\Models\Information;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $information = Information::orderBy('created_at','asc')->simplePaginate(5);
        return view('pages.index',compact('information'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = array();
        $input['name'] = $request->name;
        $input['type'] = $request->type;
        $type = $request->type;
        if($type = 'Stuff Image' ){
            $this->validate($request, [
                'image'=> 'required|image|mimes:jpeg,png,jpg|dimensions:max_width:250|max:2048'
           ]);
        }elseif($type = 'Product Image'){
            $this->validate($request, [
                'image'=> 'required|image|mimes:jpeg,png,jpg|dimensions:max_width:300|max:2048'
           ]);
        }
       

        $image = $request->file('image');
        if ($image) {
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'images/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);

            $input['image'] = $image_url;
            Information::create($input);

        return redirect()->route('information.index')
        ->with('success','Information added successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
        return view('pages.view',compact('information'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        return view('pages.edit',compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        $oldimage = $request->old_image;

        $input = array();
        $input['name'] = $request->name;
        $input['type'] = $request->type;
        

        $image = $request->file('image');
        if ($image) {
            unlink($oldimage);
            $image_name = date('dmy_H_s_i');
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'images/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);

            $input['image'] = $image_url;
            $information->update($input);
    
            return redirect()->route('information.index')
                            ->with('success','Information updated successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        $information->delete();
     
        return redirect()->route('information.index')
                        ->with('success','Information deleted successfully');
    }
}
